package com.sabre.concurrency;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarFinderCurrencyApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarFinderCurrencyApplication.class, args);
	}
}
