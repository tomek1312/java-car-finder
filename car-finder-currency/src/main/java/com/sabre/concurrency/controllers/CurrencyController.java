package com.sabre.concurrency.controllers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.concurrent.ThreadLocalRandom;

@CrossOrigin
@RestController
@RequestMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
public class CurrencyController {

    @GetMapping(path = "/{currency}")
    public BigDecimal getCurrentCurrency(@PathVariable String currency) throws InvalidRequestException {
        if (currency.equalsIgnoreCase("USD")) {
            return new BigDecimal(ThreadLocalRandom.current().nextDouble(3.0, 4.0));
        }
        if (currency.equalsIgnoreCase("EUR")) {
            return new BigDecimal(ThreadLocalRandom.current().nextDouble(4.0, 5.0));
        }
        if (currency.equalsIgnoreCase("GBP")) {
            return new BigDecimal(ThreadLocalRandom.current().nextDouble(4.0, 6.0));
        }
        throw new InvalidRequestException("Currency " + currency + " is not supported");
    }
}
