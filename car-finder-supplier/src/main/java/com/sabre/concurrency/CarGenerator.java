package com.sabre.concurrency;

import com.sabre.concurrency.model.Car;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Component
public class CarGenerator {
    public List<Car> generateCarList(String currency, int yearFrom, int yearTo, int priceFrom, int priceTo) {
        LinkedList<Car> cars = new LinkedList<>();
        cars.add(createCar("Audi", "A2", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("Audi", "A3", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("Audi", "A4", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("Audi", "A5", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("Audi", "A5", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("Audi", "A6", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("Audi", "A7", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("BMW", "Z4", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("BMW", "X1", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("BMW", "M6", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("BMW", "X5 M", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("BMW", "X3", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("BMW", "X6", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("Skoda", "Citigo", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("Skoda", "Fabia", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("Skoda", "Rapid", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("Skoda", "Octavia", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("Skoda", "Superb", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("Skoda", "Kodiaq", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("Skoda", "Kodiaq", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("Fiat", "Tipo", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("Fiat", "Uno", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("Fiat", "Uno", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("Fiat", "Panda", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));
        cars.add(createCar("Fiat", "Punto", currency, ThreadLocalRandom.current().nextInt(yearFrom, yearTo), ThreadLocalRandom.current().nextInt(priceFrom, priceTo)));

        return cars;
    }

    private Car createCar(String brand, String model, String currency, int year, int price) {
        Car car = new Car();
        car.setBrand(brand);
        car.setModel(model);
        car.setYear(year);
        car.setPrice(new BigDecimal(price));
        car.setCurrency(currency);

        return car;
    }
}
