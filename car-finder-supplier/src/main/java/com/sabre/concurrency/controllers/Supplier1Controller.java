package com.sabre.concurrency.controllers;

import com.sabre.concurrency.CarGenerator;
import com.sabre.concurrency.model.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping(value = "supplier1/car", produces = MediaType.APPLICATION_JSON_VALUE)
public class Supplier1Controller {

    @Autowired
    private CarGenerator carGenerator;
    private List<Car> cars;

    @PostConstruct
    public void init() {
        cars = carGenerator.generateCarList("USD", 1995, 2012, 1000, 20000);
    }

    @GetMapping(path = "/{brand}")
    public List<Car> getCars(@PathVariable String brand) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(30);
        return cars.stream()
                .filter(car -> car.getBrand().equalsIgnoreCase(brand))
                .collect(Collectors.toList());
    }
}
