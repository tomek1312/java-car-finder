package com.sabre.concurrency;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.stream.IntStream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = MainController.class, secure = false)
public class CarFinderApplicationTests {

	@Autowired
	private MockMvc mvc;

	@Test
	public void test() throws Exception {
		long startTime = System.currentTimeMillis();
		IntStream.range(0, 200).forEach(i -> {
			try {
				mvc.perform(get("/findCar/" + "brand").accept(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk());
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		System.out.println("Elapsed time: " + (System.currentTimeMillis() - startTime));
	}

}
