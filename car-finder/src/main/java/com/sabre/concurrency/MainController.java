package com.sabre.concurrency;

import com.sabre.concurrency.model.Car;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "findCar", produces = MediaType.APPLICATION_JSON_VALUE)
public class MainController {

    private RestTemplate restTemplate = new RestTemplate();

    @GetMapping(path = "/{brand}")
    public List<Car> getCars(@PathVariable String brand) {
        Car[] cars = restTemplate.getForObject("http://localhost:8051/supplier/supplier1/car/" + brand, Car[].class);

        return Arrays.asList(cars);
    }
}
